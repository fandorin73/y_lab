package Lesson2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
    public static void main(String[] args) throws IOException {

        // узнаем у пользователя: какое это животное, его имя, спит ли оно?

        Boolean sleepOrNotAnimal = true;
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Напишите за кем мы ведем наблюдение?(кошка, собака или утка)");
        String typeOfAnimal = reader.readLine();
        System.out.println("Напишите как зовут животное?");
        String name = reader.readLine();
        System.out.println("Напишите спит ли животное?(Да/Нет)");
        String sleepOrNot = reader.readLine();
        if (sleepOrNot.equals("Да"))
            sleepOrNotAnimal = true;
        else if (sleepOrNot.equals("Нет"))
            sleepOrNotAnimal = false;

        // создаем животное

        Animal animal = null;
        switch (typeOfAnimal) {
            case "утка":
                animal = new Duck(name, sleepOrNotAnimal);
                break;
            case "кошка":
                animal = new Cat(name, sleepOrNotAnimal);
                break;
            case "собака":
                animal = new Dog(name, sleepOrNotAnimal);
                break;
            default:
                System.out.println("Такого животного нет на ферме");
        }

        // животное подает голос, если не спит.
        // Если животное спит, спрашиваем пользователя: разбудить ли животное?


        if (sleepOrNotAnimal) {
            System.out.println(String.format("Разбудить ли %s (Да/Нет)", animal.getName()));
            sleepOrNot = reader.readLine();
            if (sleepOrNot.equals("Да"))
                animal.eat();
            else if (sleepOrNot.equals("Нет"))
                System.out.println("Хорошо, пусть спит");

        }
        else animal.voice();

    }
}
