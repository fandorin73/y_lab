package Lesson2;

public class Cat extends Animal {

    public Cat(String name, Boolean sleepOrNotAnimal) {
        super(name, sleepOrNotAnimal);
    }



    @Override
    public void voice() {

        System.out.println("Мяу-мяу");
    }
}
