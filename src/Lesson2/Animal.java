package Lesson2;

public abstract class Animal {
    // Делаем абстрактный класс
    // объединение характеристики нескольких классов

    // Инициализируем переменные
    private String name; //имя животного
    private Boolean sleepOrNotAnimal; // Спит животное или нет

    //2 вида поведения, животное либо подает голос, либо просыпаеться и ест

    public abstract void voice();

    public void eat() {
        System.out.println(String.format("Животное %s проснулась и ест", getName()));
    }

    public Animal(String name, Boolean sleepOrNotAnimal) {
        this.name = name;
        this.sleepOrNotAnimal = sleepOrNotAnimal;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getSleepOrNotAnimal() {
        return sleepOrNotAnimal;
    }

    public void setSleepOrNotAnimal(Boolean sleepOrNotAnimal) {
        this.sleepOrNotAnimal = sleepOrNotAnimal;
    }
}
